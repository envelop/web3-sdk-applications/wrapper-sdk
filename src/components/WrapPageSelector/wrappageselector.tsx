import {
	useContext,
	useEffect,
	useState
} from "react";
import {
	InfoModalContext,
	Web3Context
} from "../../dispatchers";

import WrapIndexPage from "../WrapIndexPage";
import WrapPage      from "../WrapPage/wrappage";
import config        from '../../app.config.json';
import { matchRoutes, useLocation, useNavigate } from "react-router-dom";

export default function WrapPageSelector() {

	const {
		currentChainId,
	} = useContext(Web3Context);
	const {
		setError,
	} = useContext(InfoModalContext);

	const location = useLocation();
	const navigate = useNavigate();

	const [ pageMode            , setPageMode             ] = useState<'wrap' | 'wrapindex'>('wrap');
	const [ indexContractAddress, setIndexContractAddress ] = useState<string | undefined>(undefined);
	const [ redirectIndexERC20,   setRedirectIndexERC20   ] = useState<string | undefined>(undefined);

	useEffect(() => {

		const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChainId });
		if ( !foundChain ) { return; }

		if ( foundChain.indexWrapperContract ) {
			setIndexContractAddress(foundChain.indexWrapperContract);
		} else {
			setIndexContractAddress(undefined);
		}

		const sourceParams = [
			{ path: "/index" },
		];
		const matches = matchRoutes(sourceParams, location);
		if ( matches && matches[0] && !foundChain.indexWrapperContract ) {
			setError('Unsupported chain');
			navigate('/');
		}
		if ( matches && matches[0] && foundChain.indexWrapperContract ) {
			setPageMode('wrapindex');
		} else {
			setPageMode('wrap');
		}

	}, [ currentChainId ]);

	const redirectToIndex = (address: string | undefined) => {
		if ( !address ) { return; }
		setRedirectIndexERC20(address);
		setPageMode('wrapindex');
	}

	const getPageSelector = () => {

		if ( indexContractAddress === undefined ) { return; }

		return (
			<div className="row mb-6 align-items-center">
			<div className="col-12 col-sm-auto order-md-1">
				<div className="db-section__toggle">
					<button
						className={ `tab ${ pageMode === 'wrap' ? 'active' : '' }` }
						onClick={() => { setPageMode('wrap') }}
					>
						Wrap
					</button>
					<button
						className={ `tab ${ pageMode === 'wrapindex' ? 'active' : '' }` }
						onClick={() => { setPageMode('wrapindex') }}
					>
						Index
					</button>
				</div>
			</div>
			</div>
		)
	}
	const getPage = () => {
		if ( pageMode === 'wrapindex' ) {
			return (
				<main className="s-main">
				<div className="container">

					{ getPageSelector() }

					<WrapIndexPage
						redirectIndexERC20={redirectIndexERC20}
						setRedirectIndexERC20={setRedirectIndexERC20}
					/>
				</div>
				</main>
			)
		}

		return (
			<main className="s-main">
			<div className="container">

				{ getPageSelector() }

				<WrapPage
					redirectToIndex={redirectToIndex}
				/>
			</div>
			</main>
		)
	}

	return getPage()

}