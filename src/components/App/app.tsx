
import 'tippy.js/dist/tippy.css';

import {
	ERC20Dispatcher,
	InfoModalDispatcher,
	Web3Dispatcher
} from '../../dispatchers';
import {
	matchRoutes,
	useNavigate
} from 'react-router-dom';

import Header from '../Header';
import Footer from '../Footer';

import WrapPageSelector from '../WrapPageSelector';
import InfoMessages from '../InfoMessages';

export default function App() {

	const sourceUrlParams = [
		{ path: "/:chainId/:contractAddress/:tokenId" },
		{ path: "/:chainId/:contractAddress" },
		{ path: "/:chainId" },
	];
	const matches = matchRoutes(sourceUrlParams, location);
	const navigate = useNavigate();

	return (
		<>
			<InfoModalDispatcher>
			<Web3Dispatcher switchChainCallback={(targetChainId: number) => {

				if (
					matches &&
					matches[0] &&
					matches[0].params &&
					matches[0].params.chainId
				) {
					const switchedTo = parseInt(matches[0].params.chainId);
					if ( switchedTo !== targetChainId ) { navigate('/'); }
				}
			}}>
			<ERC20Dispatcher>
				<InfoMessages />
				<Header />
				<WrapPageSelector />
				<Footer />
			</ERC20Dispatcher>
			</Web3Dispatcher>
			</InfoModalDispatcher>
		</>
	)

}